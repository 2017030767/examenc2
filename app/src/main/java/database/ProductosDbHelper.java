package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ProductosDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_PRODUCTO = " CREATE TABLE " +
            DefinirTabla.Contacto.TABLE_NAME + " (" +
            DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Contacto.CODIGO + INTEGER_TYPE + COMMA +
            DefinirTabla.Contacto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.MARCA + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.PRECIO + DOUBLE_TYPE + COMMA +
            DefinirTabla.Contacto.PERECEDERO + INTEGER_TYPE + ")";

    private static final String SQL_DELETE_PRODUCTO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Contacto.TABLE_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sistema.db";

    public ProductosDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_PRODUCTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_PRODUCTO);
        onCreate(sqLiteDatabase);
    }
}
