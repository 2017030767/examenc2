package com.example.examencorte2;

import androidx.appcompat.app.AppCompatActivity;
import database.Producto;
import database.ProductosCRUD;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;
    private ProductosCRUD db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnEditar = findViewById(R.id.btnEditar);

        db = new ProductosCRUD(this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtCodigo.getText().toString().equals("") || txtNombre.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Rellene los campos requeridos", Toast.LENGTH_SHORT).show();
                }else{
                    db.openDatabase();

                    if(db.getProductoByCodigo(Long.parseLong(txtCodigo.getText().toString())) != null){
                        Toast.makeText(MainActivity.this, "el producto ya existe", Toast.LENGTH_SHORT).show();
                    }else{
                        Producto producto = new Producto();
                        producto.setCodigo(Long.parseLong(txtCodigo.getText().toString()));
                        producto.setNombre(txtNombre.getText().toString());
                        producto.setPrecio(Double.parseDouble(txtPrecio.getText().toString()));
                        producto.setMarca(txtMarca.getText().toString());
                        producto.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero ? 1 : 0);

                        if (db.insertarProducto(producto) > 0) {
                            Toast.makeText(MainActivity.this, "Se agrego el producto con codigo " + producto.getCodigo(), Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(MainActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                        }
                        limpiar();
                    }
                    db.cerrar();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();

            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProductoActivity.class);
                startActivity(intent);
            }
        });

    }

    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        rbgPerecedero.clearCheck();
    }

}
