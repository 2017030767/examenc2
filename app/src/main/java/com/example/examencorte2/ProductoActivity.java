package com.example.examencorte2;

import androidx.appcompat.app.AppCompatActivity;
import database.Producto;
import database.ProductosCRUD;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ProductoActivity extends AppCompatActivity {
    private EditText txtCodigoB;
    private Button btnBuscar;
    private EditText txtNombreB;
    private EditText txtMarcaB;
    private EditText txtPrecioB;
    private RadioGroup rbgPerecederoB;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;
    private ProductosCRUD db;
    private Producto savedProducto = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigoB = findViewById(R.id.txtCodigoB);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombreB = findViewById(R.id.txtNombreB);
        txtMarcaB = findViewById(R.id.txtMarcaB);
        txtPrecioB = findViewById(R.id.txtPrecioB);
        rbgPerecederoB = findViewById(R.id.rbgPerecederoB);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);

        db = new ProductosCRUD(this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.openDatabase();
                Producto producto = db.getProductoByCodigo(Long.parseLong(txtCodigoB.getText().toString()));
                if(producto == null)
                {
                    Toast.makeText(ProductoActivity.this, "No se encontro el producto", Toast.LENGTH_LONG).show();
                }
                else
                {
                    savedProducto = producto;
                    txtMarcaB.setText(savedProducto.getMarca());
                    txtNombreB.setText(savedProducto.getNombre());
                    txtPrecioB.setText(String.valueOf(savedProducto.getPrecio()));
                    rbgPerecederoB.check(savedProducto.getPerecedero() == 1 ? R.id.rbPerecederoB : R.id.rbNoPerecederoB);
                }
                db.cerrar();

            }
        });
        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.openDatabase();
                db.deleteProducto(savedProducto.get_ID());
                Toast.makeText(ProductoActivity.this, "Se elimino el producto", Toast.LENGTH_LONG).show();
                db.cerrar();
                limpiar();
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.openDatabase();
                if(txtCodigoB.getText().toString().equals("") || txtNombreB.getText().toString().equals("")){
                    Toast.makeText(ProductoActivity.this, "Rellene los campos requeridos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Producto producto = new Producto();
                    producto.setCodigo(Integer.parseInt(txtCodigoB.getText().toString()));
                    producto.set_ID(savedProducto.get_ID());
                    producto.setNombre(txtNombreB.getText().toString());
                    producto.setPrecio(Double.parseDouble(txtPrecioB.getText().toString()));
                    producto.setMarca(txtMarcaB.getText().toString());
                    producto.setPerecedero(rbgPerecederoB.getCheckedRadioButtonId() == R.id.rbPerecederoB ? 1 : 0);
                    if (db.updateProducto(producto, producto.get_ID()) > 0)
                        Toast.makeText(ProductoActivity.this, "Se actualizo el producto con codigo "+ producto.getCodigo(), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(ProductoActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                    limpiar();
                }

                db.cerrar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

    }

    private void limpiar()
    {
        txtCodigoB.setText("");
        txtPrecioB.setText("");
        txtNombreB.setText("");
        txtMarcaB.setText("");
        savedProducto = null;
        rbgPerecederoB.clearCheck();

    }

}
