package database;

import java.io.Serializable;

public class Producto implements Serializable {
    private long _ID;
    private long codigo;
    private String nombre;
    private String marca;
    private Double precio;
    private int perecedero;

    public Producto(long _ID, long codigo, String nombre, String marca, Double precio, int perecedero) {
        this._ID = _ID;
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
    }

    public Producto(Producto producto) {
        this._ID = producto._ID;
        this.codigo = producto.codigo;
        this.nombre = producto.nombre;
        this.marca = producto.marca;
        this.precio = producto.precio;
        this.perecedero = producto.perecedero;
    }
    public Producto() {
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public int getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(int perecedero) {
        this.perecedero = perecedero;
    }
}
