package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductosCRUD {
    private Context context;
    private ProductosDbHelper productosDbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[]{
            DefinirTabla.Contacto._ID,
            DefinirTabla.Contacto.CODIGO,
            DefinirTabla.Contacto.NOMBRE,
            DefinirTabla.Contacto.MARCA,
            DefinirTabla.Contacto.PRECIO,
            DefinirTabla.Contacto.PERECEDERO
    };

    public ProductosCRUD(Context context) {
        this.context = context;
        this.productosDbHelper = new ProductosDbHelper(this.context);
    }

    public void openDatabase(){
        this.db = productosDbHelper.getWritableDatabase();
    }
    public long insertarProducto (Producto p){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.CODIGO, p.getCodigo());
        values.put(DefinirTabla.Contacto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Contacto.MARCA, p.getMarca());
        values.put(DefinirTabla.Contacto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Contacto.PERECEDERO, p.getPerecedero());

        return db.insert(DefinirTabla.Contacto.TABLE_NAME, null, values);
    }

    public long updateProducto (Producto p, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.CODIGO, p.getCodigo());
        values.put(DefinirTabla.Contacto.NOMBRE, p.getNombre());
        values.put(DefinirTabla.Contacto.MARCA, p.getMarca());
        values.put(DefinirTabla.Contacto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Contacto.PERECEDERO, p.getPerecedero());

        String criterio = DefinirTabla.Contacto._ID + " = " + id;

        return db.update(DefinirTabla.Contacto.TABLE_NAME, values, criterio, null);
    }

    public int deleteProducto (long id){

        String criterio = DefinirTabla.Contacto._ID + " = " + id;

        return db.delete(DefinirTabla.Contacto.TABLE_NAME, criterio, null);
    }

    public Producto readProducto(Cursor cursor){
        Producto p = new Producto();

        p.set_ID(cursor.getInt(0));
        p.setCodigo(cursor.getInt(1));
        p.setNombre(cursor.getString(2));
        p.setMarca(cursor.getString(3));
        p.setPrecio(cursor.getDouble(4));
        p.setPerecedero(cursor.getInt(5));
        return p;
    }

    public Producto getProducto (long id){
        Producto producto = null;
        SQLiteDatabase db = productosDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
                columnToRead,
                DefinirTabla.Contacto._ID + " = ? ",
                new String[] {String.valueOf(id)},
                null, null,null);
        if(c.moveToFirst()){
            producto = readProducto(c);
        }
        c.close();
        return producto;
    }

    public Producto getProductoByCodigo (long codigo){
        Producto producto = null;
        SQLiteDatabase db = productosDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
                columnToRead,
                DefinirTabla.Contacto.CODIGO + " = ? ",
                new String[] {String.valueOf(codigo)},
                null, null,null);
        if(c.moveToFirst()){
            producto = readProducto(c);
        }
        c.close();
        return producto;
    }

    public ArrayList<Producto> allProductos(){
        ArrayList<Producto> productos = new ArrayList<Producto>();
        SQLiteDatabase db = productosDbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME,
                null, null, null,
                null, null, null);
        //despues de la consulta mueve el cursor al inicio
        if(cursor != null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                Producto p = readProducto(cursor);
                productos.add(p);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return productos;
    }

    public void cerrar(){
        productosDbHelper.close();
    }

}
